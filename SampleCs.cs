﻿using UnityEngine;
using System.Collections;

public class CourtyardBoss : Boss {

    [SerializeField] GameObject mortarShot, swarmPrefab, shieldPrefab;
	public float testFloat;
	public static int TEST_INT = 3;

	public void StartStage1(int i)
    {
        StartCoroutine(RegenerateShields(15f, 1));
        StartCoroutine(LaunchMortar(60f, 1));
    }

    public static override void StartStage2(int x = 0, CustomType<Thing> test)
    {
        StartCoroutine(LaunchMortar(25f, 2));
        StartCoroutine(SpawnSwarm(25f, 2));
    }

    protected abstract void StartStage3()
    {
        StartCoroutine(LaunchMortar(5f, 2));
        StartCoroutine(SpawnSwarm(5f, 2));
    }

    IEnumerator SpawnSwarm(float seconds, int stageID)
    {
        while (this.attackID == stageID)
        {
            yield return new WaitForSeconds(seconds);
            GameObject g = Instantiate(swarmPrefab, transform.position, transform.rotation) as GameObject;
            GameObject g2 = Instantiate(swarmPrefab, transform.position, transform.rotation) as GameObject;
        }
    }


    IEnumerator RegenerateShields(float seconds, int stageID)
    {
        while(this.attackID == stageID)
        {
            
            RotatingShield[] current = FindObjectsOfType<RotatingShield>();
            int count = current.Length;
            while(count < 3)
            {
                GameObject g = Instantiate(shieldPrefab, transform.position, transform.rotation) as GameObject;
                g.transform.parent = transform;
                yield return new WaitForSeconds(1f);
                count++;
            }
            yield return new WaitForSeconds(seconds);
        }
    }

    IEnumerator LaunchMortar(float seconds, int stageID)
    {
        while (this.attackID == stageID)
        {
            yield return new WaitForSeconds(seconds);
            GameObject g = Instantiate(mortarShot, transform.position, transform.rotation) as GameObject;
        }
    }
}
