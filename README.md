# racket
The UML-to-C# constant-documentation software.

## What
Auto-generate C# classes from a UML class diagram created in draw.io.

Turn this:

[![https://gyazo.com/9438cc5301c09e518e4a20e2edc04027](https://i.gyazo.com/9438cc5301c09e518e4a20e2edc04027.png)](https://gyazo.com/9438cc5301c09e518e4a20e2edc04027)

Into this:

[![https://gyazo.com/ef596fc7659b4f73b46299294badf680](https://i.gyazo.com/ef596fc7659b4f73b46299294badf680.png)](https://gyazo.com/ef596fc7659b4f73b46299294badf680)

Without touching a line of code!

## Why

Poorly-documented projects and unclear deliverables slow down development, especially when you bring aboard new team members or have to rework old code.

Racket and other forms of automated continuous documentation (ACD) fix this problem by taking the work out of high-level documentation.

Working with Racket requires no special input - changes to classes will automatically update their associated UML diagrams and vice-versa.

## How
Racket works with a slight variation on standard UML (documented in the Naming Conventions section below) to auto-generate classes from a UML diagram designed in draw.io.

1. Start a new UML diagram in draw.io.
2. Follow the naming conventions listed below.
3. Export the resulting XML file.
4. **TODO: Continue**

### Installation
1. `npm install`

### Config Options
Racket comes with a config.js file that serves as the program options.

* **docFiles** - What your XML doc files will be called. Racket will create and maintain one doc file per directory. Glob format - see [this page](https://github.com/isaacs/node-glob#glob-primer).
* **rootPath** - The path in which Racket will start looking for files to watch.

### General Workflow
The suggested workflow for a Racket project is:

1. Set product goals and deliverables.
2. Meet among developers to determine structure of software as a whole - overarching classes, public methods, etc.
3. Place the software structure into a UML diagram on draw.io.
4. Export the UML diagram as an XML file and drop this file into your project source code.
5. Start Racket to auto-generate classes, public methods, etc. from the XML file.
6. Start Racket every time you work on your project to keep both the XML and classes up to date, giving you continuous documentation throughout your project.

### Document Construction
This is the standard UML toolset in v.5.5.8 of draw.io.

[![https://gyazo.com/c1bcda91f8c5881b8ac9a3a6034d6e4d](https://i.gyazo.com/c1bcda91f8c5881b8ac9a3a6034d6e4d.png)](https://gyazo.com/c1bcda91f8c5881b8ac9a3a6034d6e4d)

Racket was developed using a few tools in particular:

* The Class diagram (**top row, third from left**) to denote:
    * Classes
    * Interfaces
* The Generalization arrow (**bottom left hand corner**) to denote:
    * Class inheritance
    * Interface implementation

A functioning class diagram looks like this:

[![https://gyazo.com/bb932d928ce2298efe27499fe5d8a6f4](https://i.gyazo.com/bb932d928ce2298efe27499fe5d8a6f4.png)](https://gyazo.com/bb932d928ce2298efe27499fe5d8a6f4)

A diagram of an inheritance structure looks like this (**Note that the arrow points _from_ the child class _to_ the parent class.**):

[![https://gyazo.com/7afa25921b1095bb984db3317046deda](https://i.gyazo.com/7afa25921b1095bb984db3317046deda.png)](https://gyazo.com/7afa25921b1095bb984db3317046deda)

Finally, a diagram of class inheriting from another class and implementing an interface looks like this: 

[![https://gyazo.com/8bafe27e83e87baaed0788c4eaed5a3f](https://i.gyazo.com/8bafe27e83e87baaed0788c4eaed5a3f.png)](https://gyazo.com/8bafe27e83e87baaed0788c4eaed5a3f)

### Naming Conventions

You can already start to see some naming conventions appearing in the diagrams. Follow this structure to ensure that your code is generated correctly.

* Name a **class** with a single hash (`# ClassName`).
* Name an **interface** with a "greater-than" sign (`> InterfaceName`).
* Name a **public property** with a plus sign. Indicate its type *after* the name of the property. (`+ exampleFloat : float`).
* Name a **public method** with a plus sign and parentheses (`+ ExampleMethod() : void`).
	* You can return any type, pass any arguments, and declare any defaults that you normally would (`+ ExampleMethod(myString : string, myType : CustomType = default(CustomType)) : float`)
* Indicate **inheritance** or **implementation** by creating a Generalization arrow (see *Document Construction* above). 
    * Drag the arrow's start point anywhere on the inheriting/implementing class, then drag its endpoint to the base class or interface.
    * It's important that the arrow links somewhere on the class cells you have in mind, otherwise it won't be interpreted into code correctly.

* **TODO**
	* Abstract classes (italicized class/method names)
	* Virtual methods
	* Bidirectional associaton (including role name and multiplicity value)
	* Unidirectional association (including role name and multiplicity value)
	
### UML Tips and Resources
There are several resources available to learn UML concepts, including:

* [IBM article on class diagrams, the foundation of UML in C#](https://www.ibm.com/developerworks/rational/library/content/RationalEdge/sep04/bell/)

## Software Goals
* The user will install Racket at any location.
* Racket will come with one .exe/.app file and one config.js file.
* The user will be able to change definitions in config.js to change Racket options. All of these options will take place relative to the location of the config.js file in question.
* Racket will automatically create a default config.js file if one is not present.