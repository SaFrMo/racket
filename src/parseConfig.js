"use strict";

// Load modules
const	fs			= require('fs');
		
// Read config.js
let config = fs.readFileSync('./config.js');
config = JSON.parse(config);

module.exports = config;