"use strict";

// Load modules
const	fs			= require('fs'),
		xml2js		= require('xml2js'),
		createCs	= require('./createCsharp'),
		createXml	= require('./createXml'),
		glob		= require('glob'),
		config		= require('./parseConfig'),
		essence		= require('./essence/essence');
		
// Utility functions
const	isClass = function(cellElement) {
	return cellElement && cellElement.value && cellElement.value.match(/^#\s*/) != null;
}

const	isLine = function(cellElement) {
	return cellElement && cellElement.style && cellElement.hasOwnProperty('edge');
}

const	findClassParent = function(allCells, firstParentId) {
	// Find class parent of this cell
	let parent = null;
	let candidate = null;
	while ( parent != '1'){
		candidate = allCells.filter(x => x.id == ( candidate ? candidate.parent : firstParentId ) )[0];
		if ( candidate == null ) break;
		parent = candidate.parent;
	}
	return candidate;
}

const	getAllChildren = function(parentCell, allCells){
	return allCells.filter(cell => cell.parent == parentCell.id && cell.value.trim() && cell.value.trim().length);
}
		
const	createOrAppendProperty = function(sourceObject, property, value) {
	if ( sourceObject.hasOwnProperty(property) ){
		
		// Make it an array if it's not already
		if ( ! Array.isArray(sourceObject[property]) ) {
			let originalValue = sourceObject[property];
			sourceObject[property] = [originalValue];
		}
		
		// Append to array
		if (Array.isArray(value)) {
			value.forEach(x => {
				sourceObject[property].push(x);
			});
		} else {
			sourceObject[property].push(value);
		}
	} else {
		sourceObject[property] = value;
	}
	
	return sourceObject;
}

// The app!
const	app = function(){
	
	createXml.createDocs();
	return;
	
	// Open XML string
	let allXmlFiles = glob.sync(config.docFiles);
	allXmlFiles.forEach(xmlPath => {
		let xml = fs.readFileSync(xmlPath);
		
		let xmlEssence = new Essence(xmlPath, 'racket-xml');
		
		
		
		
		return;
		
	});
}




// Run app!
app();
