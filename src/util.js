"use strict";

// Load modules
const	fs			= require('fs'),
		glob		= require('glob');
		
		
// Read config.js
let config = fs.readFileSync('./config.js');
config = JSON.parse(config);

module.exports = {
	
	getAllDirectoriesIn: function(root){
		// Remove slashes - we'll add them manually
		root = root.replace('/', '');
		// Find all directories below given root
		return glob.sync('**/' + root + '/*/');
	}
	
}