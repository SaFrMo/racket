# Essence.js

A system of transforming software structures to a JSON format, which can then be translated into other formats, automatic documentation, etc.

## How
`let essence = new Essence('/path/to/file.extension');`

Create a new Essence instance by calling the constructor on the filename. Essence will create a new JSON file from the information in that file, assuming a parser for that language exists.

Currently, Essence has parsers for:

* C# (in progress)
* XML (in progress)

### Parser Conventions

#### C#

**Methods**

* Methods must start with `public`, `private`, or `protected`, and be in the following order:
    * `[public|private|protected] [static]? [virtual|abstract|override]? [returnType] [methodName] ([parameters]?)`
* *Good:* `public void GetThing()`
* *Good*: `protected static override void GetNewThing(int test, bool test2 = false)`
* *Bad:* `void GetThing()` (missing access modifier)
* *Bad:* `public virtual static void GetThing()` ('virtual' and 'static' in wrong order)

**Properties**

* Properties must start with `public`, `private`, or `protected`, and be in the following order:
    * `[public|private|protected] [static]? [virtual|abstract|override]? [propertyType] [propertyName] [= [defaultValue]]?`
* *Good:* `public int testInt;`
* *Good*: `protected static virtual CustomType COMPLICATED_THING = new CustomType();`
* *Bad:* `int thing;` (missing access modifier)
* *Bad:* `public virtual static float STATIC_FLOAT;` ('virtual' and 'static' in wrong order)

## What
An Essence object has a few basic properties, any and all of which can be left blank:

1. `name`, the name of the structure in question (for example, a class name in C#)
2. `base`, the name of the structure from which this one inherits (for example, a base class)
3. `properties`, the properties associated with a structure
4. `methods`, the methods associated with a structure
5. `guid`, a unique ID for the Essence object