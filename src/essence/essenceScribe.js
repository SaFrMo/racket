"use strict";

// Load modules
const	fs 			= require('fs'),
		scribe 		= require('./essenceScribe'),
		_			= require('underscore');

module.exports = {
	write: function(essence, templatePath){
		
		// Load template
		let template = _.template ( fs.readFileSync(templatePath, 'utf8') );
		
		let parsed = template(essence);
		console.log(essence.methods);
		console.log(parsed);
		
	},
	
	
	
	writeDep: function(essence, templatePath){
				
		// Load template
		let template = fs.readFileSync(templatePath, 'utf8');
		
		// Replace structure name and base class with Essence name and base
		let translation = template
			.replace('{{name}}', essence.name)
			.replace('{{base}}', essence.base);
			
		// Replace methods
		let methodTemplate = template.match(/{{METHODS}}([\s\S]*){{\/METHODS}}/);
		if( methodTemplate ){
			translation = translation.replace(methodTemplate[0], '{{METHODS}}');
			
			let extractedMethodTemplate = methodTemplate[0]
				.replace('{{METHODS}}', '')
				.replace('{{/METHODS}}', '');
				
			essence.methods.forEach(method => {
				let temp = extractedMethodTemplate.match(/{{(.*?)}}/g);
				let parsedMethod = extractedMethodTemplate;
				temp.forEach(placeholder => {
					let name = placeholder.match(/<(\w*)>/)[1];
					let replacer = method[name] || null;
					if( replacer != null ){
						parsedMethod = parsedMethod.replace('<' + name + '>', replacer);														
					} else {
						parsedMethod = parsedMethod.replace(placeholder, '');
					}
				});
				parsedMethod = parsedMethod.replace(/{{/g, '').replace(/}}/g, '');

				translation = translation.replace('{{METHODS}}', parsedMethod + '\n{{METHODS}}');
			});
			translation = translation.replace('{{METHODS}}', '');
		}
		
		// Replace properties
		let propertyTemplate = template.match(/{{PROPERTIES}}([\s\S]*){{\/PROPERTIES}}/g);
		if( propertyTemplate ){
			translation = translation.replace(propertyTemplate[0], '{{PROPERTIES}}');
			
			let extractedPropertyTemplate = propertyTemplate[0]
				.replace('{{PROPERTIES}}', '')
				.replace('{{/PROPERTIES}}', '');
				
			essence.properties.forEach(prop => {
				let temp = extractedPropertyTemplate.match(/{{(.*?)}}/g);
				let parsedProperty = extractedPropertyTemplate;
				temp.forEach(placeholder => {
					let name = placeholder.match(/<(\w*)>/);
					if( ! name ){
						console.log('Can\'t find property name in ' + placeholder + '. Did you enclose the name in <greater than/less than> signs?');
						return false;
					} else {
						name = name[1];
					}
					let replacer = prop[name] || null;
					if( replacer != null ){
						parsedProperty = parsedProperty.replace('<' + name + '>', replacer);														
					} else {
						parsedProperty = parsedProperty.replace(placeholder, '');
					}
				});
				parsedProperty = parsedProperty.replace(/{{/g, '').replace(/}}/g, '');

				translation = translation.replace('{{PROPERTIES}}', parsedProperty + '\n{{PROPERTIES}}');
			});
			translation = translation.replace('{{PROPERTIES}}', '');
			
		}
		
		return translation;
	}
};