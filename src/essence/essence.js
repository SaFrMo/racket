"use strict";

// Load modules
const	fs 			= require('fs'),
		scribe 		= require('./essenceScribe'),
		xml2js		= require('xml2js');

class Essence {
	
	constructor(path, fileType){
		this.path = path;
		this.raw = fs.readFileSync(path, 'utf8');
		// TODO: Error handling
		
		// Save filetype
		if( fileType == null ){
			// Grab file extension and save as file type
			let extension = path.match(/\w*$/);
			if( extension && extension.length ){
				// Save the filetype
				this.fileType = extension[0];
			} else {
				console.log('Couldn\'t find file extension! Exiting Essence instance creation.');
				return;
			}
		} else {
			this.fileType = fileType;
		}
		
		this.properties = [];
		this.methods = [];
		this.guid = Math.random().toString(36).slice(-7);
		
		// Find the correct parser and start translating into an Essence object
		this.findParser();
	}
	
	// PARSING (READING) FILES
	findParser(){
		// Parse raw file based on file type
		switch( this.fileType ){
			case 'cs' :
				this.createFromCsharp();
				break;
				
			case 'racket-xml' :
				this.createFromRacketXml();
				break;
				
			// case 'your-new-extension' :
			//		this.[your-new-parsing-function]();
			//		break;
			
			default:
				console.log('Couldn\'t find parser for filetype ' + this.fileType + '! Exiting Essence instance creation.');
				break;
		};
	}
	
	createFromCsharp(){
		
		// NOTES:
		// To be parsed successfully in Essence, a C# file must follow these conventions:
		// 1. 
		
		let keywords = [
			'if',
			'while'
		];
		
		// Save the object's name
		let nameMatch = this.raw.match(/class\s*(\w*)/);
		if( nameMatch && nameMatch.length ){
			this.name = nameMatch[1];
		} else {
			console.log('Couldn\'t find class name for ' + this.path);
		}
		
		// Save the object's base class
		let baseMatch = this.raw.match(/class\s*\w*\s*:\s*(\w*)/);
		if( baseMatch && baseMatch.length ){
			this.base = baseMatch[1];
		} else {
			console.log('Couldn\'t find base class for ' + this.path);
		}
		
		// Save this object's methods
		let methodRegex = /(public|protected|private)\s*(static)?\s*(virtual|abstract|override)?\s*(\w*)\s*(\w*)\s*\((.*)\)/g;
		let allMethods = [];
		let currentMethod = null;
		do {
			currentMethod = methodRegex.exec(this.raw);
			if( currentMethod ){
				allMethods.push({
					accessModifier: currentMethod[1],
					staticModifier: currentMethod[2] || '',
					virtualModifier: currentMethod[3] || '',
					returnType: currentMethod[4],
					name: currentMethod[5],
					parameters: currentMethod[6]
				});
			}
		} while (currentMethod);
		this.methods = allMethods;
		
		// Save this object's properties
		let propertyRegex = /\s*(public|private|protected)\s*(static)?\s*(virtual|override|abstract)?\s*(\w*)?\s*(\w*)?(?:;|\s*=?\s*(.*));/g;
		let allProperties = [];
		let currentProperty = null;
		do {
			currentProperty = propertyRegex.exec(this.raw);
			if( currentProperty ){
				allProperties.push({
					accessModifier: currentProperty[1],
					staticModifier: currentProperty[2] || '',
					virtualModifier: currentProperty[3] || '',
					propertyType: currentProperty[4],
					name: currentProperty[5],
					defaultValue: currentProperty[6] || ''
				});
			}
		} while (currentProperty);
		this.properties = allProperties;
		
		
	}
	
	
	createFromRacketXml(){
		xml2js.parseString(this.raw, (err, result) => {
			// Save all cells
			let allCells = result.mxGraphModel.root[0].mxCell;
			// xml2js stores info in json.$ field, so let's make an array of just that information
			allCells = allCells.map(cell => { 
				let toReturn = cell.$;
				if ( cell && cell.hasOwnProperty('mxGeometry') && cell.mxGeometry ){
					toReturn.mxGeometry = cell.mxGeometry[0];
				}
				return toReturn;
			});		
					
			// draw.io inserts some dummy cells at the start of the array, so let's remove them
			let dummyCount = 2;
			while (dummyCount > 0) {
				allCells.shift();
				dummyCount--;
			}
							
			// First, look for the classes
			let classes = allCells.filter(x => isClass(x));
			// Filter out null values
			classes = classes.filter(x => x);
			// Next, look for the plain lines connecting classes
			let lines = allCells.filter(x => isLine(x));
			lines = lines.filter(x => x);
			// Find annotations by looking for children of the lines
			let linesAndAnnotations = [];
			lines.forEach(line => {
				let toSave = {
					line: line,
					lineSourceClass: findClassParent(allCells, line.source),
					lineTargetClass: findClassParent(allCells, line.target),
					annotations: []
				};
				allCells.forEach(singleCell => {
					if ( singleCell.parent == line.id ){
						toSave.annotations.push(singleCell);
					}
				});
				// Save line source and target
				if ( line.hasOwnProperty('source') && line.source ){
					toSave.source = findClassParent(allCells, line.source);
				}
				if ( line.hasOwnProperty('target') && line.target ){
					toSave.target = findClassParent(allCells, line.target);
				}
				linesAndAnnotations.push(toSave);
			});
			
			// Save array of class IDs and child items to parse for each
			let parseQueue = [];
			classes.forEach(x => {
				parseQueue.push({
					ownerClass: x,
					id: x.id,
					toParse: []
				});
			});
			// Find all objects with these IDs as parents
			parseQueue.forEach(queueMember => {
				let children = getAllChildren(queueMember, allCells);
				queueMember.toParse = children;
			});
			// Parse classes in parseQueue and prep list of classes to create
			parseQueue.forEach(q => {		
				// Save classname
				let val = q.ownerClass.value;
				// Parse raw class name
				this.name = val.match(/^#\s*(\w*)/)[1];
				
				// Parse base class
				let baseClass = val.match(/.*\s*:\s*(\w*)/);
				this.base = baseClass ? baseClass[1] : 'MonoBehaviour';
				
				// Prep interfaces
				let interfaces = [];
				// Find a line with this class as its source
				let linesWithMeAsSource = linesAndAnnotations.filter(x => x.source && x.source.id == q.ownerClass.id);
				let linesWithMeAsTarget = linesAndAnnotations.filter(x => x.target && x.target.id == q.ownerClass.id);
				if( linesWithMeAsSource.length ){
					linesWithMeAsSource.forEach(singleLine => {
						
						let lineStyle = singleLine.line.style;
					
						// Find an inheritance arrow
						if( lineStyle.includes('endArrow=block') ){
							let targetValue = singleLine.target.value;
													
							if( targetValue.match(/^#/) ){
								// Handle a base class
								baseClass = targetValue.replace(/^#\s*/, '');
							} else if( targetValue.match(/^[>&gt;]/) ){
								// Handle an interface
								interfaces.push(targetValue.replace(/^[>&gt;]*\s*/, ''));
							}
						}
					});
				}
							
				// Set up worker object
				let currentObject = {
					className: className,
					baseClass: baseClass,
					interfaces: interfaces
				};
							
				// Loop through this class's toParse (array of method and property objects)
				q.toParse.forEach(p => {
					let valArr = p.value.split('+');
					let properties = valArr.map(v => {
						// Filter out methods based on parentheses
						if ( v.includes('(')) { return null; }
						// Find properties
						let m = v.match(/^\s*(\w*)\s*:\s*(\w*)/);
						return m ? m[2] + ' ' + m[1] : null;
					});
					let methods = valArr.map(v => {
						// Find methods
						let m = v.match(/^\s*(\w*)\((.*)\)\s*:\s*(\w*)/);
						let toReturn = null;
						if ( m ){
							toReturn = '';
							let returnType = m[3],
								methodName = m[1],
								allParameters = m[2].split(',');
								
							allParameters = allParameters.map(x => {
								let xMatch = x.match(/^\s*(\w*)\s*:\s*(\w*)/);
								let defaults = x.match(/=\s*(.*)/);
								if( defaults ){
									defaults = ' = ' + defaults[1];
								}
								return xMatch[2] + ' ' + xMatch[1] + (defaults || '');
							});
								
							toReturn = returnType + ' ' + methodName + '(' + allParameters.join(', ') + ')';
						}
						return toReturn;
					});
					
					// Filter out null and zero-length strings from arrays
					this.properties = properties.filter(x => x && x.length);
					this.methods = methods.filter(x => x && x.length );
					
					// TODO: START HERE
					
					// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
					// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
					
					//currentObject = createOrAppendProperty(currentObject, 'properties', properties);
					//currentObject = createOrAppendProperty(currentObject, 'methods', methods);
				});
				
				// Find associations
				linesWithMeAsSource.forEach(singleLine => {
					let lineStyle = singleLine.line.style;
					
					if ( lineStyle.includes('endArrow=none') ){
						// Find relational arrows
						// Find children
						let children = allCells.filter(x => x.parent == singleLine.line.id);
						
						let source = singleLine.source;
						let target = singleLine.target;
						
						let assoc = target.value.replace('# ', '') + ' ' + children[1].value;
						currentObject = createOrAppendProperty(currentObject, 'properties', assoc);
					}
				});
				
				linesWithMeAsTarget.forEach(singleLine => {
					let lineStyle = singleLine.line.style;
					
					if ( lineStyle.includes('endArrow=none') ){
						// Find relational arrows
						// Find children
						let children = allCells.filter(x => x.parent == singleLine.line.id);
						
						let source = singleLine.source;
						
						let assoc = source.value.replace('# ', '') + ' ' + children[0].value;
						currentObject = createOrAppendProperty(currentObject, 'properties', assoc);
					}
				});
				
				// Write the new class!
				createCs.createClass(currentObject);
			});	
			
		});
	}
	
	// TO ADD NEW PARSING LANGUAGES:
	// 	1. Add the appropriate case in findParser. See commented-out code for an example.
	//	2. Add in your new function to parse a source code file into an Essence object.
	//		Your function should convert the string `this.raw` into an Essence object.
	
	
	
	
	
	
	// TRANSLATIONS
	translateTo(filetype){
		return scribe.write(this, 'essence/templates/' + filetype + '.essence');
	}
	
}

module.exports = {
	Essence: Essence
};