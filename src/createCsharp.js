"use strict";

const	fs		= require('fs'),
		config	= require('./parseConfig');

const	readOrCreate = function(path, onExist, onCreated) {
	try {
		// File exists
		fs.accessSync(path);
		// Fire callback
		if (onExist) onExist();
	} catch (e) {
		// File doesn't exist in this directory, so create it
		fs.openSync(path, 'a+');
		// Fire callback
		if (onCreated) onCreated();
	}
}


module.exports = {
	createClass: function(traits) {		
		// Set values or defaults
		traits = {
			className: traits.className || 'YOUR-NAME-HERE',
			classDoc: traits.classDoc || '/// Your documentation here.',
			path: config.rootPath + (traits.path || (traits.className || 'YOUR-NAME-HERE') + '.cs'),
			baseClass: traits.baseClass || 'Object',
			interfaces: traits.interfaces || [],
			properties: traits.properties || [],
			methods: traits.methods || []
		};
		
		// First, read or create
		readOrCreate(traits.path, () => {
			// TODO: When file exists
		}, () => {
			let inheritance = traits.baseClass;
			traits.interfaces.forEach(interfaceName => {
				inheritance += ', ' + interfaceName;
			});
			
			// Start with properties - TODO: Documentation?
			let properties = '';
			if(traits.properties && typeof(traits.properties) != 'string'){
				traits.properties.forEach(prop => {
					let toAppend = '\tpublic ' + prop + ';\n';
					properties += toAppend;
				});
			} else {
				properties = '\tpublic ' + traits.properties + ';\n';
			}
			
			// Add methods
			let methods = '';
			traits.methods.forEach(method => {
				let toAppend = '\tpublic ' + method + ' {\n\n\t}\n\n';
				methods += toAppend;
			});
			
			// When file was just created
			let contents = fs.readFileSync('./_template.cs', 'utf-8');
			// Replace template info
			contents = contents
				.replace('{{ClassDoc}}', traits.classDoc)
				.replace('{{ClassName}}', traits.className + ' : ' + inheritance)
				.replace('{{Properties}}', properties)
				.replace('{{Methods}}', methods);
			
			// Write file
			fs.writeFileSync(traits.path, contents);
		});
	}
};