"use strict";

const	fs		= require('fs'),
		glob	= require('glob'),
		config	= require('./parseConfig'),
		util	= require('./util');

const	readOrCreate = function(path, onExist, onCreated) {
	try {
		// File exists
		fs.accessSync(path);
		// Fire callback
		if (onExist) onExist();
	} catch (e) {
		// File doesn't exist in this directory, so create it
		fs.openSync(path, 'a+');
		// Fire callback
		if (onCreated) onCreated();
	}
}

module.exports = {
	createDocs: function(){
		// Create array of all directories
		console.log(util.getAllDirectoriesIn(config.rootPath));
		
			// For each directory, find or create the docFile
			
			// Scan directory for .cs files
			
			// Create or update documentation for all .cs files
			
			// Save docFile
	},
	
	test: function(){
		
	}
};